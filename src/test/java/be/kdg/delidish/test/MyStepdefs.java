package be.kdg.delidish.test;

import be.kdg.delidish.application.Controller;

import be.kdg.delidish.domain.bestelling.*;
import be.kdg.delidish.domain.betaling.Geld;
import be.kdg.delidish.domain.betaling.Valuta;
import be.kdg.delidish.domain.common.Adres;
import be.kdg.delidish.domain.common.ContactInfo;
import be.kdg.delidish.domain.common.Positie;
import be.kdg.delidish.domain.persoon.Courier;
import be.kdg.delidish.domain.persoon.Klant;
import be.kdg.delidish.domain.persoon.LeverPuntEvent;
import be.kdg.delidish.domain.restaurant.Gerecht;
import be.kdg.delidish.domain.restaurant.Restaurant;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Before;
import org.junit.jupiter.api.BeforeAll;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class MyStepdefs {
    private static Controller controller= new Controller();
    private List<Bestelling> beschikbareOpdrachten;


    @BeforeAll
    @Given("restaurants:")
    public static void restaurants(DataTable dataTable ) {
        for(Map<String, String> i: dataTable.asMaps()){
            String naam = i.get("Name");
            float leg = Float.parseFloat(i.get("adress_long"));
            float brg = Float.parseFloat(i.get("adress_lat"));
            Positie positie = new Positie(leg,brg);
            Restaurant r = new Restaurant(Integer.parseInt(i.get("id")),naam,positie);
            controller.addRestaurant(r);
        }
    }

    @BeforeAll
    @Given("Couriers:")
    public static void couriers(DataTable dataTable) {
        for(Map<String, String> i: dataTable.asMaps()){
            String straat = i.get("Street");
            String huisnr = i.get("Number");
            Adres a = new Adres(straat,huisnr);
            String mail = i.get("mail");
            String tel = i.get("tel");
            ContactInfo ci = new ContactInfo(a,mail ,tel);
            float curLong =Float.parseFloat(i.get("current_longitude"));
            float curLat = Float.parseFloat(i.get("current_lattitude"));
            Positie p = new Positie(curLong,curLat);
            int id = Integer.parseInt(i.get("id"));
            controller.addCourier(
                    new Courier(i.get("FirstName"), i.get("LastName"), ci, p,id)
            );
        }
    }

    @BeforeAll
    @Given("customers:")
    public static void customers(DataTable dataTable) {
        for(Map<String, String> i: dataTable.asMaps()){
            ContactInfo info = new ContactInfo(i.get("mail"),i.get("tel"));
            controller.addKlant(new Klant(i.get("FirstName"),i.get("LastName"),info, controller.getAantalKlanten()));
        }
    }

    @BeforeAll
    @Given("Dishes:")
    public static void dishes(DataTable dataTable) {
        for (Map<String, String> i :
                dataTable.asMaps()) {
            Restaurant r = controller.getRestaurant(Integer.parseInt(i.get("resto_id")));
            Gerecht g = new Gerecht(Integer.parseInt(i.get("id")),i.get("name"), i.get("description"), new Geld(Valuta.EURO, Float.parseFloat(i.get("price"))), Integer.parseInt(i.get("preperationTime")), Integer.parseInt(i.get("maxDeliveryTime")), null, true, null, r);
            controller.addGerecht(r.getId(), g);
        }
    }

    @Before
    @Given("deliveryadresses:")
    public static void deliveryadresses(DataTable dataTable) {
    }

    @BeforeAll
    @Given("cities:")
    public static void cities(DataTable dataTable) {
    }

    @Given("an order with description {string} for dish with id {int} happened {int} minutes in the past and has state {string} placed by customer {int}")
    public void anOrderWithDescriptionForDishWithIdHappenedMinutesInThePastAndHasStatePlacedByCustomer(String description, int dishId, int min, String arg, int klantId) {
        controller.clearBestellingen();
        LocalDateTime datumVanBestelling = LocalDateTime.now().minusMinutes(min);
        BestellingsStatus state = null;
        if(arg.equals("ORDER_PLACED")){
            state = BestellingsStatus.BESTELLING_GEPLAATST;
        }
        Bestelling b = new Bestelling();
        BestellingsLijn bLijn = new BestellingsLijn(1,controller.getGerecht(dishId));
        BestellingsEvent be = new BestellingsEvent(b, datumVanBestelling,description,state);
        b.AddBestellingEvent(be);
        b.getBestellingsLijnList().add(bLijn);

        controller.addBestelling(b);
    }

    @And("Courier {int} is active and has {int} deliveryPoints")
    public void courierIsActiveAndHasDeliveryPoints(int courierID, int deliveryPoints) {
        Courier c = controller.getCourier(courierID);
        c.setBeschikbaar(true);
        List<LeverPuntEvent> leverPuntEventList = new LinkedList<>();
        LeverPuntEvent leverPuntEvent = new LeverPuntEvent();
        leverPuntEvent.setEventType(EventType.LEVERING_OPTIJD);//random gekozen
        leverPuntEvent.setPunten(deliveryPoints);
        leverPuntEventList.add(leverPuntEvent);
        c.setLeverPuntEvents(leverPuntEventList);
    }

    @When("Courier {int} asks for list of available deliveries")
    public void courierAsksForListOfAvailableDeliveries(int courierId) {
        beschikbareOpdrachten=controller.toonBeschikbareOpdrachten(courierId);
    }


    @Then("Courier gets an empty list")
    public void courierGetsAnEmptyList() {
        assertEquals(0,beschikbareOpdrachten.size());
    }


    @Then("Courier gets a deliverylist with {int} order")
    public void courierGetsADeliverylistWithOrder(int aantal) {
        assertEquals(aantal,beschikbareOpdrachten.size());
    }

    @And("Courier {int} selects available order {int} to deliver")
    public void courierSelectsAvailableOrderToDeliver(int idCourier, int idOrder) {
        controller.kiesLevering(idOrder,controller.getCourier(idCourier));
    }

    @And("The first delivery of the deliverylist has description {string}")
    public void theFirstDeliveryOfTheDeliverylistHasDescription(String description) {
        assertEquals(description,beschikbareOpdrachten.get(0).getBestellingsEvents().get(0).getOpmerking());
    }

    @And("Courier {int} has position {double} {double}")
    public void courierHasPosition(int courierID, double lattitude, double longitude) {
        Courier c = controller.getCourier(courierID);
        c.setHuidigePositie( new Positie( longitude, lattitude));
    }

    @Then("state of order {int} is {string}")
    public void stateOfOrderIsACCEPTED_BY_COURIER(int orderId, String state) {
        BestellingsStatus status = BestellingsStatus.valueOf(state);
        List<Bestelling>list = controller.getBestellingbyStatus(status);
        for(Bestelling b : list){
            assertEquals(status,b.getBestellingsEvents().get(b.getBestellingsEvents().size()-1).getStatus());
        }
    }

    @And("Order {int} has courier {int} assigned")
    public void orderHasCourierAssigned(int orderId, int courierId) {
        Courier c = controller.getCourier(courierId);
        for(Bestelling b : c.getBestellingen()){
            assertEquals(orderId,b.getBestellingId());
        }
    }

    @And("Courier {int} has an deliveryPoint record with type {string} with {int} points")
    public void courierHasAnDeliveryPointRecordWithTypeMISSION_ACCEPTEDWithPoints(int courierId,String state, int points) {
        Courier c = controller.getCourier(courierId);
        LeverPuntEvent gezochtEvent = null;
        for(LeverPuntEvent event:c.getLeverPuntEvents()){
            if(event.getEventType().equals(EventType.OPDRACHT_GEACCEPTEERD)){
                gezochtEvent = event;
            }
        }
        assertEquals(EventType.valueOf(state),gezochtEvent.getEventType());
        assertEquals(points,gezochtEvent.getPunten());
    }
}

