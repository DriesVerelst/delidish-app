package be.kdg.delidish.domain.bestelling;

import be.kdg.delidish.domain.persoon.Courier;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.Objects;

@Getter
@Setter
public class BestellingsEvent {
    private Bestelling bestelling;
    private LocalDateTime tijd;
    private BestellingsStatus status;
    private String opmerking;

    public BestellingsEvent(Bestelling bestelling, LocalDateTime datum, String description, BestellingsStatus state) {
        this.bestelling=bestelling;
        tijd = datum;
        opmerking = description;
        status = state;
    }

    public Bestelling getBestelling() {
        return bestelling;
    }

    public void setBestelling(Bestelling bestelling) {
        this.bestelling = bestelling;
    }

    public LocalDateTime getTijd() {
        return tijd;
    }

    public void setTijd(LocalDateTime tijd) {
        this.tijd = tijd;
    }

    public BestellingsStatus getStatus() {
        return status;
    }

    public void setStatus(BestellingsStatus status) {
        this.status = status;
    }

    public String getOpmerking() {
        return opmerking;
    }

    public void setOpmerking(String opmerking) {
        this.opmerking = opmerking;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BestellingsEvent that = (BestellingsEvent) o;
        return tijd.equals(that.tijd);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tijd);
    }
}
