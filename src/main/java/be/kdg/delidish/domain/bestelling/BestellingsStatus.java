package be.kdg.delidish.domain.bestelling;

public enum BestellingsStatus {
    BESTELLING_GEPLAATST,
    AANVAARD_DOOR_KOERIER,
    GERECHTEN_KLAAR,
    ONDERWEG_VOOR_LEVERING,
    GELEVERD
}
