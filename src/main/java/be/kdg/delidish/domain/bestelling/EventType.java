package be.kdg.delidish.domain.bestelling;

public enum EventType {
    OPDRACHT_GEACCEPTEERD,
    AFHALING_OPTIJD,
    AFHALING_NIET_OPTIJD,
    LEVERING_OPTIJD,
    LEVERING_NIET_OPTIJD,
    DAGELIJKSE_REDUCTIE
}
