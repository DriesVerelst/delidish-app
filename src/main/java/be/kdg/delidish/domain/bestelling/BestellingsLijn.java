package be.kdg.delidish.domain.bestelling;

import be.kdg.delidish.domain.restaurant.Gerecht;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class BestellingsLijn {
    private int hoeveelheid;
    private String opmerking;
    private Gerecht gerecht;

    public BestellingsLijn(int hoeveelheid, Gerecht dish) {
        this.hoeveelheid = hoeveelheid;
        gerecht = dish;
    }

    public int getHoeveelheid() {
        return hoeveelheid;
    }

    public void setHoeveelheid(int hoeveelheid) {
        this.hoeveelheid = hoeveelheid;
    }

    public String getOpmerking() {
        return opmerking;
    }

    public void setOpmerking(String opmerking) {
        this.opmerking = opmerking;
    }

    public Gerecht getGerecht() {
        return gerecht;
    }

    public void setGerecht(Gerecht gerechts) {
        this.gerecht = gerechts;
    }
}
