package be.kdg.delidish.domain.bestelling;

import be.kdg.delidish.domain.common.Adres;
import be.kdg.delidish.domain.persoon.Courier;
import be.kdg.delidish.domain.restaurant.Restaurant;
import lombok.Getter;
import lombok.Setter;

import java.lang.annotation.Retention;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class Bestelling {
    private int bestellingId;
    private Adres leveringsAdres;
    private String leveringsInstructies;
    private List<BestellingsLijn> bestellingsLijnList;
    private List<BestellingsEvent> bestellingsEvents;

    public Bestelling(){
        bestellingsEvents = new ArrayList<>();
        bestellingsLijnList = new ArrayList<>();
    }

    public void AddBestellingEvent(BestellingsEvent be) {
        bestellingsEvents.add(be);
    }

    public int getBestellingId() {
        return bestellingId;
    }

    public BestellingsEvent getLaatsteBestellingsEvent(){
        List<BestellingsEvent> gesorteerdeBestellingsEvents= bestellingsEvents.stream().sorted(Comparator.comparing(BestellingsEvent::getTijd)).collect(Collectors.toList());
        return gesorteerdeBestellingsEvents.get(gesorteerdeBestellingsEvents.size()-1);
    }

    public void setBestellingId(int bestellingId) {
        this.bestellingId = bestellingId;
    }

    public Adres getLeveringsAdres() {
        return leveringsAdres;
    }

    public void setLeveringsAdres(Adres leveringsAdres) {
        this.leveringsAdres = leveringsAdres;
    }

    public String getLeveringsInstructies() {
        return leveringsInstructies;
    }

    public void setLeveringsInstructies(String leveringsInstructies) {
        this.leveringsInstructies = leveringsInstructies;
    }

    public List<BestellingsEvent> getBestellingsEvents() {
        return bestellingsEvents;
    }

    public void setBestellingsEvents(List<BestellingsEvent> bestellingsEvents) {
        this.bestellingsEvents = bestellingsEvents;
    }

    public List<BestellingsLijn> getBestellingsLijnList() {
        return bestellingsLijnList;
    }

    public void setBestellingsLijnList(List<BestellingsLijn> bestellingsLijnList) {
        this.bestellingsLijnList = bestellingsLijnList;
    }
}
