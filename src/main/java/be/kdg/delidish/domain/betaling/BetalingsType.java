package be.kdg.delidish.domain.betaling;

public enum BetalingsType {
    DEBET,
    KREDIET
}
