package be.kdg.delidish.domain.betaling;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Geld {
    private Valuta valuta;
    private float hoeveelheid;

    public Geld(Valuta valuta, float hoeveelheid) {
        this.valuta = valuta;
        this.hoeveelheid = hoeveelheid;
    }
}
