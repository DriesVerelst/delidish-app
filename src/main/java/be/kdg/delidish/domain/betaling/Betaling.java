package be.kdg.delidish.domain.betaling;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class Betaling {
    private LocalDateTime tijd;
    private String detail;
    private Geld bedrag;
}
