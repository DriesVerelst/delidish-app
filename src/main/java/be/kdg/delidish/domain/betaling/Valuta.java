package be.kdg.delidish.domain.betaling;

public enum Valuta {
    EURO,
    DOLLAR,
    BRITSE_POND
}
