package be.kdg.delidish.domain.betaling;

import be.kdg.delidish.domain.bestelling.Bestelling;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DirecteBetaling extends Betaling {
    private String id;
    private BetalingsType betalingsType;
    private Bestelling bestelling;
}
