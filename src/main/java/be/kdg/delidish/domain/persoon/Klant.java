package be.kdg.delidish.domain.persoon;

import be.kdg.delidish.domain.bestelling.Bestelling;
import be.kdg.delidish.domain.common.Adres;
import be.kdg.delidish.domain.common.ContactInfo;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Klant extends Persoon{
    private List<Adres> leverAdressen;
    private List<Bestelling> bestellingen;

    public Klant(String firstName, String lastName, ContactInfo ci, int id) {
        super(firstName, lastName, ci, id);
    }
}
