package be.kdg.delidish.domain.persoon;

import be.kdg.delidish.domain.bestelling.Bestelling;
import be.kdg.delidish.domain.bestelling.BestellingsEvent;
import be.kdg.delidish.domain.bestelling.BestellingsLijn;
import be.kdg.delidish.domain.bestelling.BestellingsStatus;
import be.kdg.delidish.domain.common.ContactInfo;
import be.kdg.delidish.domain.common.Positie;
import be.kdg.delidish.domain.restaurant.Gerecht;
import lombok.Getter;
import lombok.Setter;
import org.locationtech.spatial4j.distance.DistanceUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class Courier extends Persoon {
    private boolean isBeschikbaar;
    private Positie huidigePositie;
    private List<Bestelling> bestellingen;
    private List<LeverPuntEvent> leverPuntEvents;

    public Courier(String firstName, String lastName, ContactInfo ci, Positie p, int id) {
        super(firstName,lastName,ci,id);
        huidigePositie = p;
        bestellingen = new ArrayList<>();
    }

    public void setBeschikbaar(boolean beschikbaar) {
        isBeschikbaar = beschikbaar;
    }

    public void setHuidigePositie(Positie huidigePositie) {
        this.huidigePositie = huidigePositie;
    }

    public void setBestellingen(List<Bestelling> bestellingen) {
        this.bestellingen = bestellingen;
    }

    public void setLeverPuntEvents(List<LeverPuntEvent> leverPuntEvents) {
        this.leverPuntEvents = leverPuntEvents;
    }

    public Positie getHuidigePositie() {
        return huidigePositie;
    }

    public List<Bestelling> getBestellingen() {
        return bestellingen;
    }

    public List<LeverPuntEvent> getLeverPuntEvents() {
        return leverPuntEvents;
    }

    public boolean isBeschikbaar() {
        return isBeschikbaar;
    }

    public Boolean komtCourierOpTijd(Bestelling b) {
        Positie p2 = b.getBestellingsLijnList().get(0)
                .getGerecht()
                .getRestaurant()
                .getPositie();
        double afstand = huidigePositie.berekenAfstand(p2);
        double afstandInKM = DistanceUtils.degrees2Dist(afstand, DistanceUtils.EARTH_EQUATORIAL_RADIUS_KM);
        double minuten = afstandInKM*4;
        Gerecht gerechtMetLangsteProductieTijd = b.getBestellingsLijnList().get(0).getGerecht();
        for(int i = 1; i<b.getBestellingsLijnList().size(); i++){
            if(gerechtMetLangsteProductieTijd.getMaxLeveringsTijd()<=b.getBestellingsLijnList().get(i).getGerecht().getMaxLeveringsTijd()){
                gerechtMetLangsteProductieTijd = b.getBestellingsLijnList().get(i).getGerecht();
            }
        }

        BestellingsEvent be = b.getBestellingsEvents().stream().filter(
                bestellingsEvent -> bestellingsEvent
                        .getStatus()== BestellingsStatus.BESTELLING_GEPLAATST)
                .findFirst().get();
        int verstrekenTijdInMinuten = LocalDateTime.now().getMinute()-be.getTijd().getMinute();
        int tijdDieCourierHeeft = gerechtMetLangsteProductieTijd.getProductieTijd()-verstrekenTijdInMinuten;

        return tijdDieCourierHeeft >= minuten;
    }





}
