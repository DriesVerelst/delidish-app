package be.kdg.delidish.domain.persoon;

import be.kdg.delidish.domain.bestelling.BestellingsEvent;
import be.kdg.delidish.domain.bestelling.EventType;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class LeverPuntEvent {
    private LocalDateTime tijd;
    private int punten;
    private EventType eventType;
    private BestellingsEvent bestellingsEvent;

    public LeverPuntEvent(LocalDateTime tijd, int punten, EventType eventType, BestellingsEvent bestellingsEvent) {
        this.tijd = tijd;
        this.punten = punten;
        this.eventType = eventType;
        this.bestellingsEvent = bestellingsEvent;
    }

    public LeverPuntEvent() {
    }

    public LocalDateTime getTijd() {
        return tijd;
    }

    public int getPunten() {
        return punten;
    }

    public EventType getEventType() {
        return eventType;
    }

    public BestellingsEvent getBestellingsEvent() {
        return bestellingsEvent;
    }

    public void setTijd(LocalDateTime tijd) {
        this.tijd = tijd;
    }

    public void setPunten(int punten) {
        this.punten = punten;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public void setBestellingsEvent(BestellingsEvent bestellingsEvent) {
        this.bestellingsEvent = bestellingsEvent;
    }
}
