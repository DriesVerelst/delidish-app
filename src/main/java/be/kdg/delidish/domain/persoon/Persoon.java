package be.kdg.delidish.domain.persoon;

import be.kdg.delidish.domain.common.ContactInfo;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
public class Persoon {
    private int id;
    private ContactInfo contactInfo;
    private String voornaam;
    private String achternaam;

    public Persoon(String firstName, String lastName, ContactInfo ci, int id) {
        voornaam=firstName;
        achternaam=lastName;
        contactInfo=ci;
        this.id = id;
    }

    public ContactInfo getContactInfo() {
        return contactInfo;
    }

    public String getVoornaam() {
        return voornaam;
    }

    public String getAchternaam(){
        return achternaam;
    }

    public int getId(){
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Persoon persoon = (Persoon) o;
        return id == persoon.id &&
                Objects.equals(voornaam, persoon.voornaam) &&
                Objects.equals(achternaam, persoon.achternaam);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, voornaam, achternaam);
    }
}
