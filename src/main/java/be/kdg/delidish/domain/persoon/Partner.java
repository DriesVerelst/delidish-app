package be.kdg.delidish.domain.persoon;

import be.kdg.delidish.domain.betaling.TransferBetaling;
import be.kdg.delidish.domain.restaurant.Restaurant;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Partner {
    private String accountNr;
    private Restaurant restaurant;
    private List<TransferBetaling> transferBetalingen;
    private Courier courier;
}
