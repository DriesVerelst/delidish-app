package be.kdg.delidish.domain.common;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Stad {
    private String postCode;
    private String stadsNaam;
}
