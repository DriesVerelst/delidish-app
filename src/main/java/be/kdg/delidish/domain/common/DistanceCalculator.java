package be.kdg.delidish.domain.common;

import org.locationtech.spatial4j.context.SpatialContext;
import org.locationtech.spatial4j.shape.Point;

public class DistanceCalculator implements IDistanceCalculator {

    public double getAfstand(Positie p1, Positie p2){
        SpatialContext ctx = SpatialContext.GEO;
        return ctx.calcDistance(p1,p2);

        // In de adapter maak je eerst een SpatialContext aan
        // Met dit object kan je daarna de afstand van twee punten berekenen
        // Zie https://locationtech.github.io/spatial4j/apidocs/ (Koppelingen naar een externe site.) voor extra documentatie
    }
}
