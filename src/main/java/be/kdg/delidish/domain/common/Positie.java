package be.kdg.delidish.domain.common;

import lombok.Getter;
import lombok.Setter;
import org.locationtech.spatial4j.context.SpatialContext;
import org.locationtech.spatial4j.shape.Point;
import org.locationtech.spatial4j.shape.Rectangle;
import org.locationtech.spatial4j.shape.Shape;
import org.locationtech.spatial4j.shape.SpatialRelation;

@Getter
@Setter
public class Positie implements Point {
    private double lengteGraad;
    private double breedteGraad;
    private DistanceCalculator distanceCalculator;

    public Positie(double lengteGraad, double breedteGraad) {
        this.lengteGraad = lengteGraad;
        this.breedteGraad = breedteGraad;
        distanceCalculator= new DistanceCalculator();
    }

    public double berekenAfstand(Positie p2) {
        return distanceCalculator.getAfstand(this,p2);
    }

    @Override
    public void reset(double x, double y) {
    }

    @Override
    public double getX() {
        return breedteGraad;
    }

    @Override
    public double getY() {
        return lengteGraad;
    }

    @Override
    public SpatialRelation relate(Shape other) {
        return null;
    }

    @Override
    public Rectangle getBoundingBox() {
        return null;
    }

    @Override
    public boolean hasArea() {
        return false;
    }

    @Override
    public double getArea(SpatialContext ctx) {
        return 0;
    }

    @Override
    public Point getCenter() {
        return null;
    }

    @Override
    public Shape getBuffered(double distance, SpatialContext ctx) {
        return null;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public SpatialContext getContext() {
        return null;
    }
}
