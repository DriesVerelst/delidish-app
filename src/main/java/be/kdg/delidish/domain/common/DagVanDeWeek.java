package be.kdg.delidish.domain.common;

public enum DagVanDeWeek {
    MAANDAG("Maandag"),
    DINSDAG("Dinsdag"),
    WOENSDAG("Woensdag"),
    DONDERDAG("Donderdag"),
    VRIJDAG("Vrijdag"),
    ZATERDAG("Zaterdag"),
    ZONDAG("Zondag");

    private String dag;

    DagVanDeWeek(String dag) {
        this.dag = dag;
    }

    @Override
    public String toString() {
        return dag;
    }
}
