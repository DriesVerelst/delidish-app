package be.kdg.delidish.domain.common;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Adres {
    private Stad stad;
    private String straat;
    private String huisNummer;
    private Positie positie;
    private String land;

    public Adres(String street, String number) {
        straat = street;
        huisNummer = number;
    }
}
