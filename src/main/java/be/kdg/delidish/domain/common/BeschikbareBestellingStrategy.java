package be.kdg.delidish.domain.common;

import be.kdg.delidish.domain.bestelling.Bestelling;
import be.kdg.delidish.domain.bestelling.BestellingsEvent;
import be.kdg.delidish.domain.bestelling.BestellingsStatus;
import be.kdg.delidish.domain.persoon.Courier;
import be.kdg.delidish.domain.persoon.LeverPuntEvent;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class BeschikbareBestellingStrategy implements IBeschikbareBestellingStrategy {
    private Courier courier;
    private List<Bestelling> bestellingList;
    private List<Courier> couriers;

    public BeschikbareBestellingStrategy(Courier courier, List<Bestelling> bestellingList, List<Courier> couriers) {
        this.courier = courier;
        this.bestellingList = bestellingList;
        this.couriers = couriers;
    }

    @Override
    public List<Bestelling> bereken(){
        List<Bestelling> beschikbareOprachten = new ArrayList<>();
        for(Bestelling b : bestellingList) {
            Boolean opTijd = courier.komtCourierOpTijd(b);
            int punten = 0;
            for (LeverPuntEvent leverPuntEvent : courier.getLeverPuntEvents()) {
                punten += leverPuntEvent.getPunten();
            }
            int avrg = berekenGemiddelde();
            int verstrekenTijdInMinuten= berekenVerstrekenTijdInMinuten(b);
            if((punten>=avrg||verstrekenTijdInMinuten>4)&&opTijd){
                beschikbareOprachten.add(b);
            }
        }

        return beschikbareOprachten;
    }

    private int berekenVerstrekenTijdInMinuten(Bestelling b){
        BestellingsEvent be = b.getBestellingsEvents().stream().filter(
                bestellingsEvent -> bestellingsEvent
                        .getStatus()== BestellingsStatus.BESTELLING_GEPLAATST)
                .findFirst().get();
        return LocalDateTime.now().getMinute()-be.getTijd().getMinute();
    }

    private int berekenGemiddelde(){
        AtomicInteger totaalPunten = new AtomicInteger();
        couriers.forEach(c -> c.getLeverPuntEvents().forEach(leverPuntEvent -> totaalPunten.addAndGet(leverPuntEvent.getPunten())));
        return totaalPunten.get() /couriers.size();
    }
}
