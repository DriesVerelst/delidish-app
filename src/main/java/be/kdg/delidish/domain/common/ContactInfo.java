package be.kdg.delidish.domain.common;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ContactInfo {
    private Adres adres;
    private String email;
    private String telefoonNummer;

    public ContactInfo(Adres a, String mail, String tel) {
        adres = a;
        email = mail;
        telefoonNummer = tel;
    }
    public ContactInfo(String mail, String tel) {
        email = mail;
        telefoonNummer = tel;
    }

    public Adres getAdres() {
        return adres;
    }

    public String getEmail() {
        return email;
    }

    public String getTelefoonNummer() {
        return telefoonNummer;
    }
}
