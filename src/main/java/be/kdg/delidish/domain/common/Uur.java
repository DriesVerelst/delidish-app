package be.kdg.delidish.domain.common;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Uur {
    private int uren;
    private int minuten;
}
