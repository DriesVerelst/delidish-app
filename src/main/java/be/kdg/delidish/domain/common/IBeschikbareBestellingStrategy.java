package be.kdg.delidish.domain.common;

import be.kdg.delidish.domain.bestelling.Bestelling;

import java.util.List;

public interface IBeschikbareBestellingStrategy {
    List<Bestelling> bereken();
}
