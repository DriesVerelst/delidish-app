package be.kdg.delidish.domain.common;

public interface IDistanceCalculator {
    public double getAfstand(Positie p1, Positie p2);
}
