package be.kdg.delidish.domain.restaurant;

public enum Allergeen {
    GLUTEN,
    EIEREN,
    VIS,
    MELK,
    NOTEN,
    SOJA,
    PINDAS,
    SELDER,
    MOSTERD,
    WEEKDIEREN,
    SCHAALDIEREN,
    LUPINE,
    SESAMZAAD,
    ZWAVELDIOXIDE
}
