package be.kdg.delidish.domain.restaurant;

import be.kdg.delidish.domain.betaling.Geld;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Gerecht {
    private int id;
    private String naam;
    private String beschrijving;
    private Geld prijs;
    private int productieTijd;
    private int maxLeveringsTijd;
    private List<Allergeen> allergenen;
    private boolean isBestelbaar;
    private List<Gerecht> gerechtsOnderdelen;
    private Restaurant restaurant;

    public Gerecht(int id,String naam, String beschrijving, Geld prijs, int productieTijd, int maxLeveringsTijd, List<Allergeen> allergenen, boolean isBestelbaar, List<Gerecht> gerechtsOnderdelen, Restaurant restaurant) {
        this.naam = naam;
        this.beschrijving = beschrijving;
        this.prijs = prijs;
        this.productieTijd = productieTijd;
        this.maxLeveringsTijd = maxLeveringsTijd;
        this.allergenen = allergenen;
        this.isBestelbaar = isBestelbaar;
        this.gerechtsOnderdelen = gerechtsOnderdelen;
        this.restaurant = restaurant;
        this.id = id;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public String getBeschrijving() {
        return beschrijving;
    }

    public void setBeschrijving(String beschrijving) {
        this.beschrijving = beschrijving;
    }

    public Geld getPrijs() {
        return prijs;
    }

    public void setPrijs(Geld prijs) {
        this.prijs = prijs;
    }

    public int getProductieTijd() {
        return productieTijd;
    }

    public void setProductieTijd(int productieTijd) {
        this.productieTijd = productieTijd;
    }

    public int getMaxLeveringsTijd() {
        return maxLeveringsTijd;
    }

    public void setMaxLeveringsTijd(int maxLeveringsTijd) {
        this.maxLeveringsTijd = maxLeveringsTijd;
    }

    public List<Allergeen> getAllergenen() {
        return allergenen;
    }

    public void setAllergenen(List<Allergeen> allergenen) {
        this.allergenen = allergenen;
    }

    public boolean isBestelbaar() {
        return isBestelbaar;
    }

    public void setBestelbaar(boolean bestelbaar) {
        isBestelbaar = bestelbaar;
    }

    public List<Gerecht> getGerechtsOnderdelen() {
        return gerechtsOnderdelen;
    }

    public void addGerechtOnderdeel(Gerecht g){
        gerechtsOnderdelen.add(g);
    }

    public Gerecht getGerechtOnderdeel(int index){
        return gerechtsOnderdelen.get(index);
    }

    public void setGerechtsOnderdelen(List<Gerecht> gerechtsOnderdelen) {
        this.gerechtsOnderdelen = gerechtsOnderdelen;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
