package be.kdg.delidish.domain.restaurant;

import be.kdg.delidish.domain.bestelling.BestellingsLijn;
import be.kdg.delidish.domain.common.ContactInfo;
import be.kdg.delidish.domain.common.Positie;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;


public class Restaurant {
    private int id;
    private String naam;
    private ContactInfo contactInfo;
    private List<OpeningsPeriode> openingsUren;
    private List<Gerecht> gerechten;
    private List<BestellingsLijn> bestellingsLijnen;
    private Positie positie;

    public Restaurant(int id, String naam, Positie positie){
        this.id = id;
        this.naam = naam;
        this.positie = positie;
        this.gerechten = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public ContactInfo getContactInfo() {
        return contactInfo;
    }

    public void setContactInfo(ContactInfo contactInfo) {
        this.contactInfo = contactInfo;
    }

    public List<OpeningsPeriode> getOpeningsUren() {
        return openingsUren;
    }

    public void setOpeningsUren(List<OpeningsPeriode> openingsUren) {
        this.openingsUren = openingsUren;
    }

    public List<Gerecht> getGerechten() {
        return gerechten;
    }

    public void setGerechten(List<Gerecht> gerechten) {
        this.gerechten = gerechten;
    }

    public List<BestellingsLijn> getBestellingsLijnen() {
        return bestellingsLijnen;
    }

    public void setBestellingsLijnen(List<BestellingsLijn> bestellingsLijnen) {
        this.bestellingsLijnen = bestellingsLijnen;
    }

    public Positie getPositie() {
        return positie;
    }

    public void setPositie(Positie positie) {
        this.positie = positie;
    }
}
