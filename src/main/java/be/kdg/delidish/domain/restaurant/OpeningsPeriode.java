package be.kdg.delidish.domain.restaurant;

import be.kdg.delidish.domain.common.DagVanDeWeek;
import be.kdg.delidish.domain.common.Uur;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OpeningsPeriode {
    private DagVanDeWeek dagVanDeWeek;
    private Uur openingsUur;
    private Uur sluitingsUur;
}
