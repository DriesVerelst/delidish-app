package be.kdg.delidish.application;

import be.kdg.delidish.business.*;
import be.kdg.delidish.domain.bestelling.Bestelling;
import be.kdg.delidish.domain.bestelling.BestellingsStatus;
import be.kdg.delidish.domain.persoon.Courier;
import be.kdg.delidish.domain.persoon.Klant;
import be.kdg.delidish.domain.restaurant.Gerecht;
import be.kdg.delidish.domain.restaurant.Restaurant;

import java.util.Collection;
import java.util.List;

public class Controller {
    private IBestellingManager bestellingManager;
    private IRestaurantManager restaurantManager;
    private IPersoonManager persoonManger;


    public Controller(){

        bestellingManager = BestsellingManager.getInstance();
        restaurantManager = RestaurantManager.getInstance();
        persoonManger = PersoonManager.getInstance();
        persoonManger.setBestellingManager(bestellingManager);
    }

    public void addBestelling(Bestelling b) {
        bestellingManager.addBestelling(b);
    }

    public void addRestaurant(Restaurant r) {
        restaurantManager.addRestaurant(r);
    }

    public void addCourier(Courier c) {
        persoonManger.addCourier(c);
    }

    public Courier getCourier(int courierID) {
        return persoonManger.getCourier(courierID);
    }

    public Klant getKlant(int klantId) {
        return persoonManger.getKlant(klantId);
    }

    public void addKlant(Klant klant) {
        persoonManger.addKlant(klant);
    }

    public int getAantCouriers() {
        return persoonManger.getAantalCouriers();
    }

    public int getAantalKlanten() {
        return persoonManger.getAantalKlanten();
    }

    public List<Bestelling> toonBeschikbareOpdrachten(int courierId) {
        return persoonManger.getBeschikbareOpdrachten(courierId);
    }

    public Collection<Klant> getAllKlanten() {
        return persoonManger.getAllKlanten();
    }

    public Restaurant getRestaurant(int restaurantId){
        return restaurantManager.getRestaurant(restaurantId);
    }

    public void addGerecht(int restaurantId,  Gerecht g) {
        restaurantManager.addGerecht(restaurantId, g);
    }

    public Gerecht getGerecht(int gerechtId) {
        for(Restaurant r : restaurantManager.getAllRestaurants()){
            for(Gerecht g : r.getGerechten()){
                if(g.getId()==gerechtId){
                    return g;
                }
            }
        }
        return null;
    }

    public void clearBestellingen() {
        bestellingManager.clearBestellingen();
    }

    public void kiesLevering(int bestellingId, Courier courier) {
        bestellingManager.kiesLevering(bestellingId,courier);
    }

    public List<Bestelling> getBestellingbyStatus(BestellingsStatus state) {
        return bestellingManager.getBestellingbyStatus(state);
    }

    public List<Bestelling> getAllBestellingen() {
        return bestellingManager.getAllBestellingen();
    }

}
