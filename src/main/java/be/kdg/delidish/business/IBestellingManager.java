package be.kdg.delidish.business;

import be.kdg.delidish.domain.bestelling.Bestelling;
import be.kdg.delidish.domain.bestelling.BestellingsStatus;
import be.kdg.delidish.domain.persoon.Courier;

import java.time.LocalDateTime;
import java.util.List;

public interface IBestellingManager {

   void addBestelling(Bestelling b);

    List<Bestelling> getBestellingbyStatus(BestellingsStatus bestellingGeplaatst);

    void clearBestellingen();

    void kiesLevering(int idOrder, Courier courier);

    List<Bestelling> getAllBestellingen();
}
