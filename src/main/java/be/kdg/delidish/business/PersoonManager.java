package be.kdg.delidish.business;

import be.kdg.delidish.domain.common.BeschikbareBestellingStrategy;
import be.kdg.delidish.domain.bestelling.Bestelling;
import be.kdg.delidish.domain.bestelling.BestellingsStatus;
import be.kdg.delidish.domain.persoon.Courier;
import be.kdg.delidish.domain.persoon.Klant;
import be.kdg.delidish.persistence.PersoonRepository;
import be.kdg.delidish.persistence.IPersoonRepository;

import java.util.Collection;
import java.util.List;

public class PersoonManager implements IPersoonManager {
    private static PersoonManager persoonManager;
    private IPersoonRepository persoonRepository;
    private IBestellingManager BestellingManager;

    private PersoonManager(){
        persoonRepository = PersoonRepository.getInstance();
    }

    public static PersoonManager getInstance() {
        if(persoonManager == null){
            persoonManager = new PersoonManager();
        }
        return persoonManager;
    }

    @Override
    public void setBestellingManager(IBestellingManager BestellingManager) {
        this.BestellingManager = BestellingManager;
    }

    @Override
    public Courier getCourier(int courierId) {
        return persoonRepository.getCourier(courierId);
    }

    @Override
    public void addCourier(Courier c) {
        persoonRepository.addCourier(c);
    }

    @Override
    public Klant getKlant(int klantId) {
        return persoonRepository.readKlant(klantId);
    }

    @Override
    public void addKlant(Klant klant) {
        persoonRepository.addKlant(klant);
    }

    @Override
    public int getAantalCouriers() {
        return persoonRepository.readAantalCouriers();
    }

    @Override
    public int getAantalKlanten() {
        return persoonRepository.readAantalKlanten();
    }

    @Override
    public List<Bestelling> getBeschikbareOpdrachten(int courierId) {
        Courier c = getCourier(courierId);
        List<Bestelling> allBestellingen =
                BestellingManager.getBestellingbyStatus(BestellingsStatus.BESTELLING_GEPLAATST);

        BeschikbareBestellingStrategy strategy = new BeschikbareBestellingStrategy(c,allBestellingen,persoonRepository.readAllCouriers());
        return strategy.bereken();
    }

    @Override
    public Collection<Klant> getAllKlanten() {
        return persoonRepository.readAllKlanten();
    }
}
