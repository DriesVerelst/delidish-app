package be.kdg.delidish.business;

import be.kdg.delidish.domain.common.Positie;
import be.kdg.delidish.domain.restaurant.Gerecht;
import be.kdg.delidish.domain.restaurant.Restaurant;
import be.kdg.delidish.persistence.IRestaurantRepository;
import be.kdg.delidish.persistence.RestaurantRepository;

import java.util.List;

public class RestaurantManager implements  IRestaurantManager{
    private static RestaurantManager restaurantManager;
    private IRestaurantRepository restaurantRepository;

    private RestaurantManager(){
        restaurantRepository = RestaurantRepository.getInstance();
    }

    public static IRestaurantManager getInstance() {
        if(restaurantManager == null){
            restaurantManager = new RestaurantManager();
        }
        return restaurantManager;
    }

    @Override
    public void addRestaurant(Restaurant r) {
        restaurantRepository.addReservatie(r);
    }

    @Override
    public Restaurant getRestaurant(int restaurantId) {
        return restaurantRepository.getRestaurant(restaurantId);
    }

    @Override
    public void addGerecht(int restaurantId, Gerecht g) {
        restaurantRepository.addGerecht(restaurantId, g);
    }

    @Override
    public List<Restaurant> getAllRestaurants() {
        return restaurantRepository.getRestauranten();
    }
}
