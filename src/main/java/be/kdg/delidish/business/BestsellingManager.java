package be.kdg.delidish.business;

import be.kdg.delidish.domain.bestelling.Bestelling;
import be.kdg.delidish.domain.bestelling.BestellingsEvent;
import be.kdg.delidish.domain.bestelling.BestellingsStatus;
import be.kdg.delidish.domain.bestelling.EventType;
import be.kdg.delidish.domain.persoon.Courier;
import be.kdg.delidish.domain.persoon.LeverPuntEvent;
import be.kdg.delidish.persistence.BestellingRepository;
import be.kdg.delidish.persistence.IBestellingRepository;

import java.time.LocalDateTime;
import java.util.List;

public class BestsellingManager implements IBestellingManager{
    private static BestsellingManager bestellingManager;
    private IBestellingRepository bestellingRepository;

    private BestsellingManager(){
        bestellingRepository = BestellingRepository.getInstance();
    }

    public static IBestellingManager getInstance() {
        if(bestellingManager == null){
            bestellingManager = new BestsellingManager();
        }
        return bestellingManager;
    }

    @Override
    public void addBestelling(Bestelling b) {
        bestellingRepository.addBestelling(b);
    }

    @Override
    public List<Bestelling> getBestellingbyStatus(BestellingsStatus bestellingGeplaatst) {
        return bestellingRepository.readBestellingbyStatus(bestellingGeplaatst);
    }

    @Override
    public void clearBestellingen() {
        bestellingRepository.clearBestellingen();
    }

    @Override
    public void kiesLevering(int bestellingID, Courier courier) {
        Bestelling b = bestellingRepository.readBestelling(bestellingID);
        BestellingsEvent be = new BestellingsEvent(b, LocalDateTime.now(),"",BestellingsStatus.AANVAARD_DOOR_KOERIER);
        b.getBestellingsEvents().add(be);
        courier.getLeverPuntEvents().add(new LeverPuntEvent(LocalDateTime.now(), 50, EventType.OPDRACHT_GEACCEPTEERD,be));
        courier.getBestellingen().add(b);
    }

    @Override
    public List<Bestelling> getAllBestellingen() {
        return bestellingRepository.readAllBestellingen();
    }


}
