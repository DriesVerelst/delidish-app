package be.kdg.delidish.business;

import be.kdg.delidish.domain.bestelling.Bestelling;
import be.kdg.delidish.domain.persoon.Courier;
import be.kdg.delidish.domain.persoon.Klant;

import java.util.Collection;
import java.util.List;

public interface IPersoonManager {
    Courier getCourier(int custommerId);

    void addCourier(Courier c);

    Klant getKlant(int klantId);

    void addKlant(Klant klant);

    int getAantalCouriers();

    int getAantalKlanten();

    List<Bestelling> getBeschikbareOpdrachten(int courierId);

    Collection<Klant> getAllKlanten();

    void setBestellingManager(IBestellingManager BestellingManager);

}
