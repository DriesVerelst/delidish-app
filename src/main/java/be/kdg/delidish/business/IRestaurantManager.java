package be.kdg.delidish.business;

import be.kdg.delidish.domain.restaurant.Gerecht;
import be.kdg.delidish.domain.restaurant.Restaurant;

import java.util.List;

public interface IRestaurantManager {

    void addRestaurant(Restaurant r);
    Restaurant getRestaurant(int restaurantId);

    void addGerecht(int restaurantId, Gerecht g);

    List<Restaurant> getAllRestaurants();
}
