package be.kdg.delidish.persistence;

import be.kdg.delidish.domain.bestelling.Bestelling;
import be.kdg.delidish.domain.bestelling.BestellingsStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class BestellingRepository implements IBestellingRepository{
    private static BestellingRepository bestellingRepository;
    private List<Bestelling> bestellingen;

    private BestellingRepository(){
        bestellingen = new ArrayList<>();
    }

    public static BestellingRepository getInstance() {
        if(bestellingRepository == null){
            bestellingRepository = new BestellingRepository();
        }
        return bestellingRepository;
    }

    @Override
    public Bestelling readBestelling(int id) {
        return bestellingen.stream().filter(bestelling -> bestelling.getBestellingId() == id).findAny().get();
    }

    @Override
    public List<Bestelling> readBestellingbyStatus(BestellingsStatus bestellingGeplaatst) {

        List<Bestelling> list = bestellingen.stream().filter(
                bestelling -> bestelling
                        .getLaatsteBestellingsEvent()
                        .getStatus()==bestellingGeplaatst)
                .collect(Collectors.toList());
        return list;
    }

    @Override
    public void addBestelling(Bestelling b) {
        bestellingen.add(b);
    }

    @Override
    public void clearBestellingen() {
        bestellingen.clear();
    }

    @Override
    public List<Bestelling> readAllBestellingen() {
        return bestellingen;
    }
}
