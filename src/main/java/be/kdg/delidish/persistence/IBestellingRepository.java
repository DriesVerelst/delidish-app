package be.kdg.delidish.persistence;

import be.kdg.delidish.domain.bestelling.Bestelling;
import be.kdg.delidish.domain.bestelling.BestellingsStatus;

import java.util.List;

public interface IBestellingRepository {
    Bestelling readBestelling(int id);

    List<Bestelling> readBestellingbyStatus(BestellingsStatus bestellingGeplaatst);

    void addBestelling(Bestelling b);

    void clearBestellingen();

    List<Bestelling> readAllBestellingen();
}
