package be.kdg.delidish.persistence;

import be.kdg.delidish.domain.persoon.Courier;
import be.kdg.delidish.domain.persoon.Klant;

import java.util.Collection;
import java.util.List;

public interface IPersoonRepository {
    Courier getCourier(int custommerId);

    void addCourier(Courier courier);

    Klant readKlant(int klantId);

    void addKlant(Klant klant);

    int readAantalCouriers();

    int readAantalKlanten();

    List<Courier> readAllCouriers();

    Collection<Klant> readAllKlanten();
}
