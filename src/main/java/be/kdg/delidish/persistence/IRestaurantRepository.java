package be.kdg.delidish.persistence;

import be.kdg.delidish.domain.restaurant.Gerecht;
import be.kdg.delidish.domain.restaurant.Restaurant;

import java.util.List;

public interface IRestaurantRepository{
    void addReservatie(Restaurant r);
    Restaurant getRestaurant(int id);
    List<Restaurant> getRestauranten();
    void addGerecht(int restaurantId, Gerecht g);
}
