package be.kdg.delidish.persistence;

import be.kdg.delidish.domain.restaurant.Gerecht;
import be.kdg.delidish.domain.restaurant.Restaurant;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

public class RestaurantRepository implements IRestaurantRepository {
    private static RestaurantRepository restaurantRepository;
    private List<Restaurant> restaurants;

    private RestaurantRepository(){
        restaurants = new ArrayList<>();
    }

    public static RestaurantRepository getInstance() {
        if(restaurantRepository == null){
            restaurantRepository = new RestaurantRepository();
        }
        return restaurantRepository;
    }
    @Override
    public void addReservatie(Restaurant r) {
        if(!restaurants.contains(r)){
            restaurants.add(r);
        }
    }

    @Override
    public Restaurant getRestaurant(int id) {
        return restaurants.get(id);
    }

    @Override
    public List<Restaurant> getRestauranten() {
        return restaurants;
    }

    @Override
    public void addGerecht(int restaurantId, Gerecht g) {
        Restaurant r = getRestaurant(restaurantId);
        r.getGerechten().add(g);
    }
}
