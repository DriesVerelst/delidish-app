package be.kdg.delidish.persistence;

import be.kdg.delidish.domain.persoon.Courier;
import be.kdg.delidish.domain.persoon.Klant;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class PersoonRepository implements IPersoonRepository {
    private static PersoonRepository courierRepository;
    private List<Courier> couriers;
    private List<Klant> klanten;

    private PersoonRepository(){
        couriers = new ArrayList<>();
        klanten = new ArrayList<>();
    }

    public static IPersoonRepository getInstance() {
        if(courierRepository == null){
            courierRepository = new PersoonRepository();
        }
        return courierRepository;
    }

    @Override
    public Courier getCourier(int courierId){
        return couriers.stream().filter(courier -> courier.getId() == courierId).findAny().get();
    }

    @Override
    public void addCourier(Courier courier) {
        synchronized (couriers){
            if(couriers.size()==0){
                couriers.add(courier);
            }
            if(!couriers.contains(courier)){
                couriers.add(courier);
            }
        }

    }

    @Override
    public Klant readKlant(int klantId) {
        return klanten.stream().filter(k -> k.getId() == klantId).findAny().get();
    }

    @Override
    public void addKlant(Klant klant) {
        klanten.add(klant);
    }

    @Override
    public int readAantalCouriers() {
        return couriers.size();
    }

    @Override
    public int readAantalKlanten() {
        return klanten.size();
    }

    @Override
    public List<Courier> readAllCouriers() {
        return couriers;
    }

    @Override
    public Collection<Klant> readAllKlanten() {
        return klanten;
    }
}
